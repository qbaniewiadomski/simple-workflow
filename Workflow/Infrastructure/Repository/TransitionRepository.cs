﻿using System;
using Workflow.Domain.Enum;
using Workflow.Domain.Repository;
using Workflow.Infrastructure.Model;

namespace Workflow.Infrastructure.Repository
{
    public class TransitionRepository: TransitionRepositoryInterface
    {
        public TransitionRepository()
        {
        }

        public StateEnum[] getAvailableStates(StateEnum from)
        {
            foreach(State state in this.getAllStates())
            {
                if(from == state.getName())
                {
                    return state.getAvailableTransitions();
                }
            }

            return new StateEnum[] { };
        }

        private State[] getAllStates()
        {
            return new State[] {
                new State(StateEnum.NEW_ORDER, new StateEnum[] {StateEnum.WAITING_FOR_PAYMENT}),
                new State(StateEnum.WAITING_FOR_PAYMENT, new StateEnum[] {StateEnum.ORDER_PAID}),
                new State(StateEnum.ORDER_PAID, new StateEnum[] {StateEnum.READY_TO_COMPLETE}),
                new State(StateEnum.READY_TO_COMPLETE, new StateEnum[] {StateEnum.COMPLETING_ORDER}),
                new State(StateEnum.COMPLETING_ORDER, new StateEnum[] {StateEnum.READY_TO_COMPLETE, StateEnum.PACKING_ORDER}),
                new State(StateEnum.PACKING_ORDER, new StateEnum[] {StateEnum.ORDER_SENT}),
                new State(StateEnum.ORDER_SENT, new StateEnum[] {StateEnum.ORDER_DELIVERED}),
                new State(StateEnum.ORDER_DELIVERED, new StateEnum[] {StateEnum.ORDER_FINISHED}),
            };
        }
    }
}

