﻿using System;
using Workflow.Domain.Enum;

namespace Workflow.Infrastructure.Model
{
    public class State
    {
        private StateEnum name;
        private StateEnum[] availableTransitions;

        public State(StateEnum name, StateEnum[] availableTransitions)
        {
            this.name = name;
            this.availableTransitions = availableTransitions;
        }

        public StateEnum getName()
        {
            return this.name;
        }

        public StateEnum[] getAvailableTransitions()
        {
            return this.availableTransitions;
        }

    }
}

