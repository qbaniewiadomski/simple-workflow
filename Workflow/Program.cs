﻿using Workflow.Application;
using Workflow.Domain.Service.Engine;
using Workflow.Domain.Service.Validator;
using Microsoft.Extensions.DependencyInjection;
using Workflow.Domain.Repository;
using Workflow.Infrastructure.Repository;

var builder = new ServiceCollection()
    .AddSingleton<Application, Application>()
    .AddSingleton<WorkflowEngineInterface, WorkflowEngine>()
    .AddSingleton<WorkflowValidatorInterface, WorkflowValidator>()
    .AddSingleton<TransitionRepositoryInterface, TransitionRepository>()
    .BuildServiceProvider();

Application app = builder.GetRequiredService<Application>();
app.run();