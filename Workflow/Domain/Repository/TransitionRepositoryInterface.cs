﻿using System;
using Workflow.Domain.Enum;

namespace Workflow.Domain.Repository
{
    public interface TransitionRepositoryInterface
    {
        StateEnum[] getAvailableStates(StateEnum from);
    }
}

