﻿using System;
using Workflow.Domain.Enum;

namespace Workflow.Domain.Exception
{
    public class NoTransitionsAvailableException : System.Exception
    {
        public NoTransitionsAvailableException()
        {
        }

        public NoTransitionsAvailableException(string message)
        : base(message)
        {
        }

        public NoTransitionsAvailableException(string message, System.Exception inner)
            : base(message, inner)
        {
        }

        public static NoTransitionsAvailableException withState(StateEnum from)
        {
            string text = String.Format("There are no transitions from state [{0}]", from.ToString());

            return new NoTransitionsAvailableException(text);
        }
    }
}

