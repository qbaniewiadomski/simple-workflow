﻿using Workflow.Domain.Enum;

namespace Workflow.Domain.Exception
{
    public class TransitionNotPossibleException: System.Exception
    {
        public TransitionNotPossibleException()
        {
        }

        public TransitionNotPossibleException(string message)
        : base(message)
        {
        }

        public TransitionNotPossibleException(string message, System.Exception inner)
            : base(message, inner)
        {
        }

        public static TransitionNotPossibleException withStates(
            StateEnum from,
            StateEnum to
            )
        {
            string text = String.Format("Transition from [{0}] to [{1}] is not possible!", from.ToString(), to.ToString());

            return new TransitionNotPossibleException(text);
        }
    }
}

