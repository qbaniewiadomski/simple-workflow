﻿using System;
namespace Workflow.Domain.Enum
{
    public enum StateEnum
    {
        NEW_ORDER,
        WAITING_FOR_PAYMENT,
        ORDER_PAID,
        READY_TO_COMPLETE,
        COMPLETING_ORDER,
        PACKING_ORDER,
        ORDER_SENT,
        ORDER_DELIVERED,
        ORDER_FINISHED
    }
}

