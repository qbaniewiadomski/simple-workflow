﻿using System;
using Workflow.Domain.Enum;

namespace Workflow.Domain.Service.Validator
{
    public interface WorkflowValidatorInterface
    {
        bool hasAnyTransition(StateEnum state);

        bool isTransitionPossible(StateEnum from, StateEnum to);
    }
}

