﻿using System;
using Workflow.Domain.Enum;
using Workflow.Domain.Repository;

namespace Workflow.Domain.Service.Validator
{
    public class WorkflowValidator: WorkflowValidatorInterface
    {
        private TransitionRepositoryInterface transitionRepository;

        public WorkflowValidator(TransitionRepositoryInterface transitionRepository)
        {
            this.transitionRepository = transitionRepository;
        }

        public bool hasAnyTransition(StateEnum state)
        {
            StateEnum[] availableStates = this.transitionRepository.getAvailableStates(state);

            return availableStates.Count() > 0;
        }

        public bool isTransitionPossible(StateEnum from, StateEnum to)
        {
            StateEnum[] availableStates = this.transitionRepository.getAvailableStates(from);

            return availableStates.Contains(to);
        }
    }
}

