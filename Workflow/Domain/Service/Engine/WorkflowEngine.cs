﻿using System;
using Workflow.Domain.Enum;
using Workflow.Domain.Exception;
using Workflow.Domain.Model;
using Workflow.Domain.Service.Validator;

namespace Workflow.Domain.Service.Engine
{
    public class WorkflowEngine : WorkflowEngineInterface
    {
        private WorkflowValidatorInterface workflowValidator;

        public WorkflowEngine(WorkflowValidatorInterface workflowValidator)
        {
            this.workflowValidator = workflowValidator;
        }

        public StateEnum getInitialState()
        {
            return StateEnum.NEW_ORDER;
        }

        public void process(WorkflowObjectInterface workflowObject, StateEnum targetState)
        {
            StateEnum sourceState = workflowObject.State;
            if (!this.workflowValidator.hasAnyTransition(sourceState))
            {
                throw NoTransitionsAvailableException.withState(sourceState);
            }

            if (!this.workflowValidator.isTransitionPossible(sourceState, targetState))
            {
                throw TransitionNotPossibleException.withStates(sourceState, targetState);
            }

            workflowObject.State = targetState;
        }
    }
}

