﻿using System;
using Workflow.Domain.Enum;
using Workflow.Domain.Model;

namespace Workflow.Domain.Service.Engine
{
    public interface WorkflowEngineInterface
    {
        StateEnum getInitialState();

        void process(WorkflowObjectInterface workflowObject, StateEnum targetState);
    }
}

