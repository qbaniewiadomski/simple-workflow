﻿using System;
using Workflow.Domain.Enum;

namespace Workflow.Domain.Model
{
    public interface WorkflowObjectInterface
    {
        public StateEnum State { get; set; }
    }
}

