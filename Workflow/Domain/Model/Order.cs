﻿using System;
using System.Xml.Linq;
using Workflow.Domain.Enum;

namespace Workflow.Domain.Model
{
    public class Order: WorkflowObjectInterface
    {
        private StateEnum state;

        public Order(StateEnum state)
        {
            this.state = state;
        }

        public StateEnum State {
            get { return state; }
            set { state = value; } 
        }
    }
}

