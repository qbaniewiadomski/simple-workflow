﻿using System;
using Workflow.Domain.Enum;
using Workflow.Domain.Exception;
using Workflow.Domain.Model;
using Workflow.Domain.Service.Engine;

namespace Workflow.Application
{
    public class Application
    {
        private WorkflowEngineInterface engine;
        private WorkflowObjectInterface workflowObject;

        public Application(WorkflowEngineInterface engine)
        {
            this.engine = engine;
            this.workflowObject = new Order(engine.getInitialState());
        }

        public void run()
        {

            Console.WriteLine("Write target state code:");
            string? input;
            while((input = Console.ReadLine()) != null)
            {
                StateEnum targetState;
                if(!Enum.TryParse<StateEnum>(input, out targetState))
                {
                    Console.WriteLine("Unknown state, write state code correctly");
                    continue;
                }
                try
                {
                    engine.process(workflowObject, targetState);
                }
                catch(TransitionNotPossibleException e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
                catch(NoTransitionsAvailableException e)
                {
                    Console.WriteLine(e.Message);
                    break;
                }
            } 

        }
    }
}

